const mutations = `
  type Mutation {
    newUser(name: String!): User!
  }
`;

export default mutations;
