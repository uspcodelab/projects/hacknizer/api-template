// import used models
import User from "../../db/models/user.js";

export default {
  Query: {
    user() {
      return { id: 1, name: "Victor" }
    }
  },
  Mutation: {
    async newUser(parent, args, context, info) {
      await User.sync();

      const { dataValues } = await User.create({
        name: args.name,
      });

      console.log(dataValues);

      return dataValues;
    }
  }
}
